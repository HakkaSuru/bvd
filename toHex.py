__version__ = 3.0

__meta__ = """
Author: HakkaSuru
Description: This Script converts the last 4 characters of Salesforce ID to hex
"""

import argparse
import sys
import csv
import os
import time

class bcolors:
	HEADER = '\033[95m'
	PERCENTAGE = '\033[96m'
	OKGREEN = '\033[92m'
	WARNING = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	NOTE = '\033[93m'

def chorus(file):

	if ".csv" not in file:
		print bcolors.WARNING + 'Error: ' + file + " is not in csv format or doesn't belong in input folder, please remove. :(" + bcolors.ENDC
	else:
	
		header = []
		columns = []
		read_bytes = 0
		
		if os.path.exists('input') == False:
			print bcolors.WARNING + "Error: Create folder named \"input\" and put csv filess in it. :)" + bcolors.ENDC
			sys.exit(0)
		if os.path.exists('output') == False:
			print bcolors.WARNING + "Error: Create folder named \"output\". :)" + bcolors.ENDC
			sys.exit(0)
		
		input = open('input\\' + file, "rb")
		output = open('output\\' + file, "wb")
		
		size = os.stat('input\\' + file).st_size

                #input = open('input//' + file, "rb")
                #output = open('output//' + file, "wb")

                #size = os.stat('input//' + file).st_size


		#print "file size: " + str(size)
		
		try:
			print bcolors.HEADER + 'Reading file: ' + file + bcolors.ENDC
			reader = csv.reader(input)
			writer = csv.writer(output)
			for row in reader:
				#print row
				read_bytes += len(','.join(row)+'\r\n')
				#print read_bytes
				cal = str(int((float(read_bytes)/size)*100))
				sys.stdout.write(bcolors.PERCENTAGE + "Running: %s%%		 "%(cal) + bcolors.ENDC + '\r')
				sys.stdout.flush()
				if row != None:
					if not header:
						header = row
						for value in header:
							if "ID" in value:
								columns.append(header.index(value))
						#print columns
						for value in columns:
							header.append(header[value]+'_toHex')
						#print header
						writer.writerow(header)
						#writer.writerow(tuple(header))
					else:
						line = row
						for value in columns:
							hexlist = []
							#for char in row[value][-4:]:
							for char in row[value][9:13]:
								hexlist.append(char.encode('hex'))
							line.append(''.join(hexlist))
						#print tuple(line)
						writer.writerow(line)
						#writer.writerow(tuple(line))
				#time.sleep(0.5)
			print ''
			print bcolors.OKGREEN + 'Done :)' + bcolors.ENDC
		finally:
			input.close()
			output.close()

#------------------------------------main------------------------------------------
parser = argparse.ArgumentParser(prog='toHex.py',usage='%(prog)s [options]', description='ID to Hex')
parser.add_argument('-f', '--file', help='', type=str, dest='filename')
parser.add_argument('-a', '--all', help='', action='store_true')
parser.add_argument('-v', '--version', help='', action='version', version='%(prog)s ' + str(__version__))
	
args = parser.parse_args()

if len(sys.argv) < 2:
	parser.print_help()
	sys.exit(0)
	
if args.filename:
	
	print bcolors.NOTE + 'Note: do not close cmd, press ctrl-z, press ctrl-c while running thank you :)' + bcolors.ENDC
	chorus(args.filename)
		
if args.all:

	filelist = []
	
	if os.path.exists('input') == False:
		print bcolors.WARNING + "Error: Create folder named \"input\" and put csv filess in it. :)" + bcolors.ENDC
		sys.exit(0)
	
	for file in os.listdir('input'):
		if os.path.isfile('input\\' + file):
			filelist.append(file)
			
	if filelist == None:
		print bcolors.WARNING + 'Error: No files found' + bcolors.ENDC
	else:
		print bcolors.NOTE + 'Note: do not close cmd, press ctrl-z, press ctrl-c while running thank you :)' + bcolors.ENDC
		for file in filelist:
			chorus(file)
